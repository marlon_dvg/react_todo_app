const admin = require("firebase-admin");
var serviceAccount = require("../todoapp-e4501-firebase-adminsdk-3f6kx-22854b5eb0.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://todoapp-e4501.firebaseio.com"
});

const db = admin.firestore();

module.exports = { admin, db };